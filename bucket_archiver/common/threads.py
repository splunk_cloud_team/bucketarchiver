#
# @author Michael Doyle
# @email mdoyle@splunk.com
# @create date 2020-05-04 18:50:08
# @modify date 2020-05-04 18:50:08
# @desc [description]
#
import logging
import os
from threading import currentThread


def workerThread(worker_queue, transfer_queue, args):

    myName = currentThread().getName()
    logger = logging.getLogger(myName)
    logger.debug("{}".format("hello"))

    count = 0
    while True:
        bucket = worker_queue.get()
        
        if bucket == "__DONE__":
            worker_queue.put("__DONE__")
            worker_queue.task_done()
            logger.info("Thread done.")
            break # while

        logger.debug("{}".format(bucket))
        logger.debug("bucket title {}".format(bucket.title))
        logger.debug("{}".format(bucket.data))

        logger.info("Starting Bucket: {}".format(bucket.title))
        
        index = next(item for item in args.indexList if item['title'] == bucket.data['index'])

        if bucket.data['standalone'] == 1:
            search_pattern = "_{}".format(bucket.data['id'])
            logger.debug(
                "Looking for stand alone bucket *{}".format(search_pattern))

        else:
            search_pattern = "_{}_{}".format(
                bucket.data['id'], bucket.data['guid'])
            logger.debug(
                "Looking for clustered bucket *{}".format(search_pattern))

        # this part is really inefficient.
        try:
            bucket_path = next(dir for dir in os.listdir(
                index['coldPath']) if dir.endswith(search_pattern) and not dir.startswith('DIS'))
        except StopIteration:
            try:
                bucket_path = next(dir for dir in os.listdir(
                    index['homePath']) if dir.endswith(search_pattern) and not dir.startswith('DIS'))
            except StopIteration:
                bucket_path = None
                logger.warning(
                    "Could NOT find bucket directory for {} on the filesystem".format(bucket.title))
            else:
                os_path = index['homePath']
        else:
            os_path = index['coldPath']

        if bucket_path == None:
            full_path = None
            
            if bucket.data['retries'] >= 5:
                logger.critical("Could not find bucket {} on the filesystem after {} retries.".format(
                    bucket.title, bucket.data['retries']))
            else:
                logger.warning(
                    "Could not find bucket {} on the filesystem -- it may have rolled from warm to cold -- will retry later".format(bucket.title))
                bucket.data['retries'] += 1
                worker_queue.put(bucket)
                worker_queue.task_done()
                continue

        elif bucket_path.startswith("hot"):
            logger.warning(
                "Found hot bucket {} -- skipping".format(bucket_path))
            full_path = None

        else:
            try:
                _, endTime, startTime, _, guid = bucket_path.split("_")
            except ValueError:
                guid = args.my_guid
                _, endTime, startTime, _ = bucket_path.split("_")
            
            # if startTime < args.startup_times[guid]: 
            
            # if startTime < args.startup_time:
            
            bucket.update(full_path=os.path.join(
                os_path, bucket_path), bucket_root=bucket_path)
            logger.debug("Found bucket {} at {}".format(
                bucket.title, bucket.data['full_path']))

            # perhaps compute the checksum here?
            # maybe split the bucket into files to increase throughput?
            # process bucket here
            transfer_queue.put(bucket)
            # else:
            #     logger.warning("Skipping bucket created after restart {}".format(bucket_path))
                
        logger.info("Completed processing bucket {} -- approximately {} buckets remaining".format(
            bucket.title, worker_queue.qsize()))
        worker_queue.task_done()
