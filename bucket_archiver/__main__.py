#
# @author Michael Doyle
# @email mdoyle@splunk.com
# @create date 2020-05-04 13:46:34
# @modify date 2021-05-05 18:11:02
# @desc [description]
#
from __future__ import division
import json
import logging
import logging.handlers
import errno
import multiprocessing
import os
import pickle
import sys
import time
from threading import Lock, Thread, current_thread

from .common.threads import workerThread
from .common.utils import (SplunkMigrationBucket, getIndexerPosition,
                           getMyGUID, getMyIndexes, getMyInstanceType,
                           getMyPrimaryBuckets, getServerInfo, getSessionKey, getPeers)

try:
    from queue import Queue
except ImportError:
    from Queue import Queue

def logListenerConfig(debug=False, verbose=False):
    LOG_DIR = "log"
    root = logging.getLogger()
    root.setLevel(logging.NOTSET)
    try:
        os.makedirs(LOG_DIR)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    # Formatters
    f_verbose = logging.Formatter('%(asctime)s %(levelname)8s [ %(name)s:%(funcName)s() ] file="%(filename)s", line_no="%(lineno)s", %(message)s', datefmt="%Y-%m-%dT%H:%M:%S%z")
    f_console = logging.Formatter('%(levelname)8s - %(message)s', datefmt="%Y-%m-%dT%H:%M:%S%z")
    f_file = logging.Formatter('%(asctime)15s %(levelname)8s [ %(name)s ] - %(message)s', datefmt="%Y-%m-%dT%H:%M:%S%z")

    # Handlers
    if debug:
        h_debug = logging.handlers.RotatingFileHandler(os.path.normpath(os.path.join(LOG_DIR, "debug.log")), mode="a", maxBytes=50000000, backupCount=0)
    # else:
    #     h_debug = logging.FileHandler(os.path.normpath(os.path.join(LOG_DIR, "debug.log")), mode="w")

        h_debug.setFormatter(f_verbose)
        h_debug.setLevel(logging.DEBUG)
        root.addHandler(h_debug)

    h_console = logging.StreamHandler()
    h_console.setLevel(logging.INFO)

    if verbose:
        h_console.setFormatter(f_file)

    else:
        h_console.setFormatter(f_console)

    h_error = logging.FileHandler(os.path.normpath(os.path.join(LOG_DIR, "errors.log")), mode="a", delay=True)
    h_error.setFormatter(f_file)
    h_error.setLevel(logging.ERROR)

    h_info = logging.FileHandler(os.path.normpath(os.path.join(LOG_DIR,"output.log")), mode="a", delay=True)
    h_info.setFormatter(f_file)
    h_info.setLevel(logging.INFO)

    root.addHandler(h_console)
    root.addHandler(h_error)
    root.addHandler(h_info)

def archive(args):
    logger = logging.getLogger(__name__)
    logger.debug("args=\"{}\"".format(vars(args)))

    if (args.instance_type == 'SLAVE'):
        logger.info("Getting list of peers from CM")
        # args.startup_times = getPeers(args)
        logger.info("Getting list of primary buckets from {}".format(args.clustermaster))

    else:
        args.startup_times = {args.my_guid: args.startup_time}
        logger.info("Getting list of all local buckets")

    args.resume = False

    if os.path.isfile("bucket_list.sav"):
        if sys.version_info[0] > 2:
            resume=input("Found existing bucket list. Recover from file? (yes/no): ")
        else:
            resume=raw_input("ound existing bucket list. Recover from file? (yes/no): ")

        if resume.startswith("Y") or resume.startswith("y"):
            args.resume = True

    if args.resume:
        logger.info("Getting previous bucket list from bucket_list.sav")
        try:
            with open("bucket_list.sav", "rb") as f:
                bucketList = pickle.load(f)
        except:
            raise SystemExit("--resume specified, but file does not exist")

    else:
        try:
            bucketList = getMyPrimaryBuckets(args)

        except:
            msg = "Could not get list of primary buckets."
            logger.exception(msg)
            raise SystemExit(msg)

    with open("bucket_list.sav", "wb") as f:
        pickle.dump(bucketList, f, pickle.HIGHEST_PROTOCOL)

    fn = "bucket-list-{}.json".format(args.my_guid)

    with open(fn, "w") as f:
        for item in bucketList:
            f.write(json.dumps({'timestamp': time.time(), 'title': item.title, 'data': item.data}))
            f.write(",\n")

    for item in bucketList:
        logger.debug(item.title)

    worker_queue = Queue()

    # multiprocessing.set_start_method('spawn')
    transfer_queue = multiprocessing.JoinableQueue(2)
    workers = []
    for i in range(args.workers):
        worker = Thread(target=workerThread, name="migrateSnowballWorker-{}".format(i), args=(worker_queue, transfer_queue,  args,))
        worker.setDaemon(True)
        worker.start()
        workers.append(worker)

    if args.dest == 's3' or args.dest == 'snowball':
        from bucket_archiver.common.s3 import transferWorker
    else:
        raise SystemExit("Destination {} not supported".format(args.dest))

    if __name__ ==  '__main__':

        lock = multiprocessing.Lock()
        t_agents = []
        for i in range(args.workers):
            transfer_agent = multiprocessing.Process(target=transferWorker, args=(lock, transfer_queue, args,))
            transfer_agent.daemon = True
            transfer_agent.start()
            t_agents.append(transfer_agent)

    # with open("missing.source", "r") as f:
    #         itemList = f.read().splitlines()

    # filter bucketlist to only buckets in the file
    # bucketList = list(filter(lambda x: not x.data['index'].startswith("_"), bucketList))
    # for item in list(filter(lambda x: x.title in itemList, bucketList)):

    for item in bucketList:
        # if item.data['index_et'] < args.startup_time:

        logger.debug("adding {} to queue".format(item.title))
        worker_queue.put(item)

    # worker_queue.put("__DONE__")

    # for thread in workers:
    #     thread.join()

    worker_queue.join()

    try:
        logger.info("waiting for transfers to complete. {} items in queue".format(transfer_queue.qsize()))

    except NotImplementedError:
        logger.info("waiting for transfers to complete.")

    transfer_queue.put("__DONE__")

    for process in t_agents:
        process.join()

    logger.info("All transfers complete -- cleaning up")

    logger.info("Done")

    return True

def restore(args):
    logger = logging.getLogger(__name__)
    logger.debug("hello, args={}".format(vars(args)))

    if args.my_no is None or args.idx_count is None:
        args.my_no, args.idx_count = getIndexerPosition(args)

    logger.debug(vars(args))

    if args.src == 's3':
        from .common.s3 import getMyRestoreBuckets, getMyRestoreBuckets1
        from .common.s3 import downloadWorker

    # bucketList = []
    # bucketList = getMyRestoreBuckets(args)

    # try:
    #     assert len(bucketList) > 0
    # except AssertionError:
    #     logger.critical("bucketList is empty")
    #     raise SystemExit()

    # process_count = (lambda x: int(x//1.5) if x>3 else 1)(multiprocessing.cpu_count())
    # process_count = (lambda x: int(x*16) if x>3 else 1)(multiprocessing.cpu_count())
    process_count = 70
    download_queue = multiprocessing.JoinableQueue(10000)

    if __name__ ==  '__main__':
        logger.debug(__name__)
        logger.debug(vars(args))
        lock = multiprocessing.Lock()

        # process_count = (lambda x: int(x//1.5) if x>3 else 1)(multiprocessing.cpu_count())
        # process_count = (lambda x: int(x*4) if x>3 else 1)(multiprocessing.cpu_count())

        for _i in range(process_count):
            download_agent = multiprocessing.Process(target=downloadWorker, name="downloadWorker-{}".format(_i), args=(lock, download_queue, args,))
            download_agent.daemon = True
            download_agent.start()

    for item in getMyRestoreBuckets1(args):
        logger.info("adding {} to queue".format(item.title))
        download_queue.put(item)

    try:
        logger.info("waiting for transfers to complete. {} items in queue".format(download_queue.qsize()))
    except NotImplementedError:
        pass

    download_queue.join()

    logger.info("All transfers complete -- cleaning up")
    time.sleep(1)
    download_queue.join()

    logger.info("Done")

    return True

    # for item in all_buckets:
    #     if (item['id'] % idx_count) == my_no:
            # this is for me
            # figure out where it goes on my filesystem
            # put on transfer queue

    # q.join()

    # done





def config(args):
    logger = logging.getLogger(__name__)
    logger.debug("hello, args={}".format(vars(args)))
    print("config")


def run(args, username, password):

    if args.debug:
        logListenerConfig(debug=True, verbose=False)
    elif args.verbose:
        logListenerConfig(debug=False, verbose=True)
    else:
        logListenerConfig(debug=False, verbose=False)

    logger = logging.getLogger(__name__)
    logger.debug("args=\"{}\"".format(vars(args)))

    # for any action run the following
    logger.info("Authenticating to local instance")

    try:

        args.my_token = getSessionKey("https://localhost:{}".format(args.local_port), username, password)

    except:

        msg = "Authentication Failure"
        logger.critical(msg)
        sys.exit(msg)

    logger.debug("Got Session Key {}".format(args.my_token))

    try:
        args.instance_type, args.my_guid, args.clustermaster, args.cluster_type, args.server_name, args.startup_time = getServerInfo(args)

    except:
        raise

    if not args.instance_type == 'MASTER':
        args.indexList = []

        try:
            args.indexList = getMyIndexes(args)

        except:
            msg = "Error getting index list"
            logger.critical(msg)
            raise SystemExit(msg)

        fn = "index-list-{}.json".format(args.my_guid)

        with open(fn, "w") as f:
            f.write(json.dumps({'timestamp': time.time(), 'indexList': args.indexList}))

        logger.info("Running in {} environment".format(args.cluster_type))

    else:
        logger.debug("Instance Type {} -- skipping index list".format(args.instance_type))

    if (args.instance_type == 'SLAVE'):
        logger.debug("Authentication to cluster master {}".format(args.clustermaster))

        try:
            args.cm_token = getSessionKey(args.clustermaster, username, password)

        except:
            msg = "Authentication Error"
            logger.exception(msg)
            raise SystemExit(msg)

    if args.action == "restore":
        restore(args)

    elif args.action == "archive":
        archive(args)

    elif args.action == "config":
        if args.instance_type == 'MASTER':
            config(args)
        else:
            msg = "config is only appropriate to run on the Cluster Master -- this node is {}".format(args.instance_type)
            logger.critical(msg)
            raise SystemExit(msg)








if __name__ == '__main__':
    # import argparse
    from getpass import getpass
    import sys
    from .args import getArgs

    # cliParser = argparse.ArgumentParser(prog="bucket_archiver", description="A utility to archive and restore Splunk buckets")
    # # cliParser.add_argument('--local_port', type=int, help='The local REST port (default 8089)', default=8089)
    # # cliParser.add_argument('--verbose', action='store_true')
    # # cliParser.add_argument('--debug', action='store_false')
    # # cliParser.add_argument('-w', '--workers', type=int, help='Number of worker threads (more is not always better)', default=2)
    # # cliParser.add_argument('-t', '--test', help='Don\'t actually execute the copy operation', action='store_true')

    # global_parser = argparse.ArgumentParser(add_help=False)
    # global_parser.add_argument('--local_port', type=int, help='The local REST port (default 8089)', default=8089)
    # global_parser.add_argument('--verbose', action='store_true')
    # global_parser.add_argument('--debug', action='store_true')
    # global_parser.add_argument('-w', '--workers', type=int, help='Number of worker threads (more is not always better)', default=2)
    # global_parser.add_argument('-t', '--test', help='Don\'t actually execute the copy operation', action='store_true')
    # global_parser.add_argument('--no_skip', help='Don\'t skip internal indexes', action='store_true')
    # global_parser.add_argument('--clustermaster', help='(required for indexer clusters) uri for the cluster master <ip>|<dns>:port')
    # global_parser.add_argument('--rename_buckets', help='If enabled will rename the buckets to the index~id~GUID format on copy', action='store_true')
    # global_parser.add_argument('--add_guid_to_path', help='If enabled will add a prefix identifying the source of the bucket (for historical reasons)', action='store_true')



    # s3_shared_parser = argparse.ArgumentParser(add_help=False)
    # s3_required = s3_shared_parser.add_argument_group('S3 Requrired')
    # s3_optional = s3_shared_parser.add_argument_group('S3 Optional')
    # s3_required.add_argument('--access_key', help='aws S3 access key', required=True)
    # s3_required.add_argument('--secret_key', help='aws S3 secret access key', required=True)
    # s3_required.add_argument('--bucket_name', help='aws S3 bucket name', required=True)
    # s3_optional.add_argument('--bucket_path', help='S3 object key prefix (aka path)', default='/')
    # s3_optional.add_argument('--endpoint_url', help='aws S3 endpoint url', default='https://s3.amazonaws.com')

    # snowball_shared_parser = argparse.ArgumentParser(add_help=False)
    # snowball_required = snowball_shared_parser.add_argument_group('Snowball Required')
    # snowball_optional = snowball_shared_parser.add_argument_group('Snowball Optional')
    # snowball_required.add_argument('--access_key', help='Snowball access key', required=True)
    # snowball_required.add_argument('--secret_key', help='Snowball secret access key', required=True)
    # snowball_required.add_argument('--bucket_name', help='Snowball bucket name', required=True)
    # snowball_required.add_argument('--endpoint_url', help='Snowball endpoint url', required=True)
    # snowball_optional.add_argument('--bucket_path', help='Snowball object key prefix (aka path)', default='/')

    # config_shared_parser = argparse.ArgumentParser(add_help=False)
    # config_required = config_shared_parser.add_argument_group('Required')
    # config_optional = config_shared_parser.add_argument_group('Optional')

    # cmd_parser = cliParser.add_subparsers(help='Action', dest="action")

    # archive_parser = cmd_parser.add_parser('archive', help="Copy buckets to remote location")

    # dest_parser = archive_parser.add_subparsers(help="Destination Type", dest="dest")

    # s3_dest_parser = dest_parser.add_parser('s3', help="Amazon S3 destination", parents=[s3_shared_parser, global_parser], conflict_handler='resolve')
    # snowball_dest_parser = dest_parser.add_parser('snowball', help="Amazon Snowball destination", parents=[snowball_shared_parser, global_parser], conflict_handler='resolve')
    # azure_dest_parser = dest_parser.add_parser('azure', help='not implemented', parents=[global_parser], conflict_handler='resolve' )
    # gcp_dest_parser = dest_parser.add_parser('gcp', help='not implemented', parents=[global_parser], conflict_handler='resolve' )
    # flat_dest_parser = dest_parser.add_parser('flat', help='not implemented', parents=[global_parser], conflict_handler='resolve' )


    # # RESTORE
    # restore_parser = cmd_parser.add_parser('restore', help="Restore copied buckets to a Splunk Indexer")

    # src_parser = restore_parser.add_subparsers(help="Source Type", dest="src")

    # s3_src_parser = src_parser.add_parser('s3', help="Amazon S3 source", parents=[s3_shared_parser, global_parser], conflict_handler='resolve')




    # config_parser = cmd_parser.add_parser('config', help="Configure the Cluster Master and check connectivity")

    # args = cliParser.parse_args()

    args = getArgs()

    print("Provide the authentication information for Splunk")

    if sys.version_info[0] > 2:
        username=input("Enter username (must have 'admin' Splunk role assigned): ")
    else:
        username=raw_input("Enter username (must have 'admin' Splunk role assigned): ")

    password=getpass()

    run(args, username, password)